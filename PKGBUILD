# Maintainer: Alfin Bakhtiar Ilhami (alf) <alfin@nuclea.id>
# Maintainer: Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Maintainer: Tobias Powalowski <tpowa@archlinux.org>
# Contributor: Thomas Baechler <thomas@archlinux.org>

_srcname=linux-5.3
pkgver=5.3.1
pkgrel=1
_zenver=$pkgver-zen1
pkgbase=linux-cappucino
arch=(x86_64)
url="https://cdn.kernel.org"
_zenurl='https://github.com/zen-kernel/zen-kernel/releases/download'
license=(GPL2)
makedepends=(xmlto kmod inetutils bc libelf git)
options=('!strip')
source=(
	$_srcname.tar.xz::$url/pub/linux/kernel/v5.x/$_srcname.tar.xz
	$_srcname.tar.sign::$url/pub/linux/kernel/v5.x/$_srcname.tar.sign
	linux-$_zenver.patch.xz::$_zenurl/v$_zenver/v$_zenver.patch.xz
	kernel-{60.hook,firmware.txt,config}
	# linux-$pkgver.patch.xz::$url/pub/linux/kernel/v5.x/patch-$pkgver.xz
)
validpgpkeys=(
	'ABAF11C65A2970B130ABE3C479BE3E4300411886' # Linus Torvalds
	'647F28654894E3BD457199BE38DBBDC86092693E' # Greg Kroah-Hartman
)
_kernelname=${pkgbase#linux}

prepare() {
	# mount-secret
	cd $_srcname

	msg2 "Applying zen patch: $_zenver..."
	patch -Np1 <"../linux-$_zenver.patch"
	# patch -Np1 <"../linux-$pkgver.patch"

	msg2 'Copying private key...'
	cp '/home/ultraman/.rahasia/module/nuclea-keypub.pem' './signing_key.pem'

	msg2 'Setting config...'

	# cat '../kernel-config' >.config
	# cat '../configs/latest' >.config
	# make localmodconfig
	zcat /proc/config.gz >.config

	local FW=$(grep "^[^#]" ../kernel-firmware.txt | tr '\n' ' ')
	# local FW_DIR="../linux-firmware-$_firmver"

	sed -i "
		s|CONFIG_EXTRA_FIRMWARE=.*|CONFIG_EXTRA_FIRMWARE=\"$FW\"|g
		" .config
	# s|CONFIG_EXTRA_FIRMWARE_DIR=.*|CONFIG_EXTRA_FIRMWARE_DIR\"$FW_DIR\"|g

	make prepare
	# make xconfig
	# make prepare

	msg2 "Setting version..."
	scripts/setlocalversion --save-scmversion

	echo "-$pkgrel" >localversion.10
	make EXTRAVERSION=.zen -s kernelrelease >../version
	msg2 "Prepared %s version %s" "$pkgbase" "$(<../version)"
}

build() {
	msg2 "Building kernel and modules..."
	cd $_srcname
	make EXTRAVERSION=.zen bzImage modules
}

_package() {
	pkgdesc="The ${pkgbase/linux/Linux} kernel and modules"
	depends=(coreutils linux-firmware kmod mkinitcpio)
	optdepends=('crda: to set the correct wireless channels of your country')
	install=kernel.install

	local kernver="$(<version)"
	local modulesdir="$pkgdir/usr/lib/modules/$kernver"

	cd $_srcname

	msg2 "Installing boot image..."
	# systemd expects to find the kernel here to allow hibernation
	# https://github.com/systemd/systemd/commit/edda44605f06a41fb86b7ab8128dcf99161d2344
	install -Dm644 "$(make -s image_name)" "$modulesdir/vmlinuz"

	msg2 "Signing boot image..."
	/usr/local/lib/nuclea-tools/sign-boot -m "$modulesdir/vmlinuz"
	install -Dm644 "$modulesdir/vmlinuz" "$pkgdir/boot/bzImage_cappucino.efi"

	msg2 "Installing modules..."
	make EXTRAVERSION=.zen INSTALL_MOD_PATH="$pkgdir/usr" modules_install

	# a place for external modules,
	# with version file for building modules and running depmod from hook
	local extramodules="extramodules$_kernelname"
	local extradir="$pkgdir/usr/lib/modules/$extramodules"
	install -Dt "$extradir" -m644 ../version
	ln -sr "$extradir" "$modulesdir/extramodules"

	# remove build and source links
	rm "$modulesdir"/{source,build}

	msg2 "Installing hooks..."
	# sed expression for following substitutions
	local subst="
		s|%PKGBASE%|$pkgbase|g
		s|%KERNVER%|$kernver|g
		s|%EXTRAMODULES%|$extramodules|g
	"

	# hack to allow specifying an initially nonexisting install file
	sed "$subst" "$startdir/$install" >"$startdir/$install.pkg"
	true && install=$install.pkg

	# fill in pacman hooks
	sed "$subst" ../kernel-60.hook | install -Dm644 /dev/stdin \
		"$pkgdir/usr/share/libalpm/hooks/60-$pkgbase.hook"

	# sed "$subst" ../kernel-90.hook | install -Dm644 /dev/stdin \
	# "$pkgdir/usr/share/libalpm/hooks/90-$pkgbase.hook"

	# sed "$subst" ../kernel.preset | install -Dm644 /dev/stdin \
	# "$pkgdir/usr/share/libalpm/hooks/$pkgbase.preset"

	msg2 "Fixing permissions..."
	chmod -Rc u=rwX,go=rX "$pkgdir"
}

_package-headers() {
	pkgdesc="Header files and scripts for building modules for ${pkgbase/linux/Linux} kernel"

	local builddir="$pkgdir/usr/lib/modules/$(<version)/build"

	cd $_srcname

	msg2 "Installing build files..."
	install -Dt "$builddir" -m644 Makefile .config Module.symvers System.map vmlinux
	install -Dt "$builddir/kernel" -m644 kernel/Makefile
	install -Dt "$builddir/arch/x86" -m644 arch/x86/Makefile
	cp -t "$builddir" -a scripts

	# add objtool for external module building and enabled VALIDATION_STACK option
	install -Dt "$builddir/tools/objtool" tools/objtool/objtool

	# add xfs and shmem for aufs building
	mkdir -p "$builddir"/{fs/xfs,mm}

	# ???
	mkdir "$builddir/.tmp_versions"

	msg2 "Installing headers..."
	cp -t "$builddir" -a include
	cp -t "$builddir/arch/x86" -a arch/x86/include
	install -Dt "$builddir/arch/x86/kernel" -m644 arch/x86/kernel/asm-offsets.s

	install -Dt "$builddir/drivers/md" -m644 drivers/md/*.h
	install -Dt "$builddir/net/mac80211" -m644 net/mac80211/*.h

	msg2 "Installing KConfig files..."
	find . -name 'Kconfig*' -exec install -Dm644 {} "$builddir/{}" \;

	msg2 "Removing unneeded architectures..."
	local arch
	for arch in "$builddir"/arch/*/; do
		[[ $arch == */x86/ ]] && continue
		echo "Removing $(basename "$arch")"
		rm -r "$arch"
	done

	msg2 "Removing documentation..."
	rm -r "$builddir/Documentation"

	msg2 "Removing broken symlinks..."
	find -L "$builddir" -type l -printf 'Removing %P\n' -delete

	msg2 "Removing loose objects..."
	find "$builddir" -type f -name '*.o' -printf 'Removing %P\n' -delete

	msg2 "Stripping build tools..."
	local file
	while read -rd '' file; do
		case "$(file -bi "$file")" in
		application/x-sharedlib\;*) # Libraries (.so)
			strip -v $STRIP_SHARED "$file" ;;
		application/x-archive\;*) # Libraries (.a)
			strip -v $STRIP_STATIC "$file" ;;
		application/x-executable\;*) # Binaries
			strip -v $STRIP_BINARIES "$file" ;;
		application/x-pie-executable\;*) # Relocatable binaries
			strip -v $STRIP_SHARED "$file" ;;
		esac
	done < <(find "$builddir" -type f -perm -u+x ! -name vmlinux -print0)

	msg2 "Adding symlink..."
	mkdir -p "$pkgdir/usr/src"
	ln -sr "$builddir" "$pkgdir/usr/src/$pkgbase-$pkgver"

	msg2 "Fixing permissions..."
	chmod -Rc u=rwX,go=rX "$pkgdir"
}

pkgname=(
	"$pkgbase"
	"$pkgbase-headers"
)

for _p in "${pkgname[@]}"; do
	eval "package_$_p() {
		$(declare -f "_package${_p#$pkgbase}")
		_package${_p#$pkgbase}
	}"
done

sha256sums=('78f3c397513cf4ff0f96aa7d09a921d003e08fa97c09e0bb71d88211b40567b2'
	'SKIP'
	'b4e1050c4142f506bb674f9587f8d5800e10af7aec5a521ff3178c2296a4bc21'
	'50d8d8c6ed8f44c2c42c977e3f39a3e737fd3520479985f4c914b7729931e4f2'
	'8ee1fb9f710161b4fbea23a6a301957829aefe48c230a78502a195462bc64d6f'
	'3a63acd2a7137a5dd4e9d5b1a0ccd8059b4ceb56a0287e576a2f7bff88eff877')
