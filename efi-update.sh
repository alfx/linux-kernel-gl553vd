update_efi() {
	local LABEL LABELS LOADER LOADERS CMDLINE CMDLINES CMDLINE_DEFAULT CMDLINE_VENDOR CMDLINE_ARCH CMDLINE_CAPPUCINO CMDLINE_VANILLA CMDLINE_ZEN

	LABELS=(
		'Vanilla'
		'Cappucino'
		'Arch'
		'Zen'
	)

	LOADERS=(
		'bzImage_vanilla.efi'
		'bzImage_cappucino.efi'
		'vmlinuz-linux.signed'
		'vmlinuz-linux-zen.signed'
	)

	CMDLINE_DEFAULT='root=PARTLABEL=root rw rootfstype=f2fs rootflags=lazytime,nodiscard i915.enable_guc=3 i915.fastboot=1 quiet nvidia-drm.modeset=1 nvidia.NVreg_UsePageAttributeTable=1'
	CMDLINE_VENDOR="$CMDLINE_DEFAULT nvidia-drm.modeset=1 nvidia.NVreg_UsePageAttributeTable=1"
	CMDLINE_VANILLA="$CMDLINE_DEFAULT"
	CMDLINE_CAPPUCINO="$CMDLINE_DEFAULT"
	CMDLINE_ARCH="$CMDLINE_VENDOR initrd=initramfs-linux.img"
	CMDLINE_ZEN="$CMDLINE_VENDOR initrd=initramfs-linux-zen.img"

	CMDLINES=(
		"$CMDLINE_VANILLA"
		"$CMDLINE_CAPPUCINO"
		"$CMDLINE_ARCH"
		"$CMDLINE_ZEN"
	)

	for i in $@; do
		# efibootmgr -Bb $i -q
		LABEL="${LABELS[$i]}"
		LOADER="${LOADERS[$i]}"
		CMDLINE="${CMDLINES[$i]}"
		echo "Index      : $i"
		echo "Label      : $LABEL"
		echo "Loader     : $LOADER"
		echo "Parameters : $CMDLINE"
		echo
		efibootmgr --quiet --create-only \
			--label "$LABEL" \
			--loader "$LOADER" \
			--unicode "$CMDLINE"
		echo
	done
	# efibootmgr -o 0,1,2,3
}

update_efi $@
